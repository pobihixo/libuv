const rp = require('request-promise');

const getPipelines = async (projectId, status) => {
    const options = {
        uri: `https://gitlab.com/api/v4/projects/${projectId}/pipelines`,
        qs: {
            status
        },
        json: true,
        headers: {
            'PRIVATE-TOKEN': process.env.TOKEN
        }
    };

    return await rp(options);
};

const triggerPipeline = async (projectId) => {
    const options = {
        uri: `https://gitlab.com/api/v4/projects/${projectId}/pipeline`,
        method: 'POST',
        json: true,
        qs: {
            ref: 'master'
        },
        headers: {
            'PRIVATE-TOKEN': process.env.TOKEN
        }
    };

    return rp(options);
};

const getProject = async () => {
    const options = {
        uri: `https://gitlab.com/api/v4/projects`,
        qs: {
            owned: true
        },
        json: true,
        headers: {
            'PRIVATE-TOKEN': process.env.TOKEN
        }
    };

    const res = await rp(options);
    return res[0].id;
};


const main = async () => {
    const Total            = 50;
    const max              = 4;
    const projectId        = await getProject();
    const runningPipelines = await getPipelines(projectId, 'running');
    const pendingPipelines = await getPipelines(projectId, 'pending');
    const amountToRun      = Total - runningPipelines.length - pendingPipelines.length;
    for (var i = 0; i < amountToRun && i < max; i++) {
        await triggerPipeline(projectId);
    }
};


main();


